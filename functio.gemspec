#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require_relative 'lib/functio/version'

Gem::Specification.new do |s|
  s.name = 'functio'
  s.version = Functio::VERSION
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.1.9'
  s.summary = 'Turn your recurrent calculations into functions'
  s.description = <<-EOS
Functio is a calculator that allows you to create and manage functions for your
recurrent calculations.
EOS
  s.author = 'Cassiano Kuplich'
  s.email = 'crkuplich@openmailbox.org'
  s.homepage = 'https://gitlab.com/functio/functio'
  s.license = 'GPL-3.0+'

  s.files = `git ls-files -z`.split("\0")
  s.executables << 'fn'

  s.add_runtime_dependency 'dentaku', '2.0.8'
  s.add_runtime_dependency 'thor', '~> 0.19'
  s.add_development_dependency 'bundler', '~> 1.11'
end
