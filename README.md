# Functio - Turn your recurrent calculations into functions

Functio is a calculator that allows you to create and manage functions for your
recurrent calculations.

## Installation

Functio is written in [Ruby](https://www.ruby-lang.org/) and is packaged with
[RubyGems](https://rubygems.org/). You need Ruby version 2.1.9 or later
installed in your system. The recommended way is using [RVM](http://rvm.io/) to
install and manage Ruby versions.

With Ruby installed, run the following command:

    $ gem install functio

## Usage

Functio provides a set of commands for function management and calculation. Run
`fn help` or just `fn` to get the list of all Functio commands:

    $ fn help

The following example helps you to get started.

### Example

Suppose you want to know the balance of your savings account for a given period
in the future. Given that your savings account pays a fixed interest rate per
month, you can calculate the balance using the compound interest formula:

    principal * (1 + interest) ^ period

    where:
      principal is the principal sum
      interest is the interest rate
      period is the length of time

This formula is also useful to simulate your other investments. You can use
Functio to remember this formula for you. First, create a function using the
compound interest formula:

    $ fn create compInterest 'principal * (1 + interest) ^ period'

The command `create` creates a new function receiving the new function name and
its definition. In the example above, `compInterest` is the function name and
`'principal * (1 + interest) ^ period'` is the function definition which is the
formula for compound interest.

The new `compInterest` function is now available for use. You can see your
created functions with the command `list`:

    $ fn list
    > compInterest <principal> <interest> <period>

It also shows the function parameters. In the example above, `<principal>`,
`<interest>`, and `<period>` are the `compInterest` parameters. Finally, to use
your new awesome function run the `use` command:

    $ fn use compInterest 2500 0.02 6
    > 2815.40604816

The command `use` receives the function name and the function parameters in the
order shown by the `list` command. In the example above, `2500` is the
`principal`, `0.02` is the `interest`, and `6` is the `period`. You calculated
the balance of your savings account after 6 months for the amount of 2500 with a
monthly interest rate of 2%.

If you don't want to use the `compInterest` function anymore and want to delete
it, run the `delete` command:

    $ fn delete compInterest
    > Deleted function 'compInterest'

### Quick calculations

Functio also provides the command `eval` for expression evaluation. It can be
used as a calculator:

    $ fn eval '(80 * 2 + (3 + 1) * 2) / 4'
    > 42

## Built-in operators and functions

Currently, Functio uses [Dentaku](https://github.com/rubysolo/dentaku) Ruby gem
for mathematical and logical evaluation and parsing, so Functio's built-in
operators and functions available are the same provided by Dentaku. See the
[Dentaku's built-in operators and
functions](https://github.com/rubysolo/dentaku#built-in-operators-and-functions)
list.

## Contributing

Bug reports and merge requests are welcome on GitLab at
https://gitlab.com/functio/functio.

## Functio License

Copyright (C) 2016  Cassiano Rocha Kuplich

Functio is free software: you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation, either version 3 of the License, or  
(at your option) any later version.

Functio is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
[GNU General Public License](COPYING) for more details.
