#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'csv'
require 'fileutils'

module Functio
  # Manages data storage. DataStorage handles each record data as a Hash, so it
  # expects Hashes when stores data and returns Hashes when retrieves data.
  class DataStorage
    # Directory path of data file.
    attr_reader :data_dir

    # Path of data file.
    attr_reader :data_file

    def initialize # :nodoc:
      @data_dir = File.join(ENV['HOME'], '.functio')
      @data_file = File.join(data_dir, 'functions.csv')
    end

    # Stores a +record+ Hash. The keys of +record+ Hash are the field names and
    # the values are the field values.
    def store(record)
      FileUtils.mkdir_p(data_dir)
      CSV.open(data_file, 'a') do |csv|
        csv.seek(0, IO::SEEK_END)
        csv << record.keys if csv.pos == 0
        csv << record.values
      end
    end

    # Finds a record that matches exactly the +fields+ Hash passed. The keys of
    # +fields+ Hash are the field names and the values are the field values to
    # match exactly with the record searched for.
    def find(fields)
      found = table.find { |row| row_matches?(row, fields) }
      found.to_h if found
    end

    # Returns all stored records as an Array of Hashes. Each Hash is a record.
    def all
      table.map(&:to_h)
    end

    # Deletes a record that matches exactly the +fields+ passed in. See
    # DataStorage#find for +fields+ Hash description. Returns +true+ if the
    # record matching +fields+ is deleted successfully or +false+ otherwise.
    def delete(fields)
      tbl = table
      row_idx = tbl.find_index { |row| row_matches?(row, fields) }
      return false unless row_idx
      tbl.delete(row_idx)
      if tbl.empty?
        FileUtils.remove_dir(data_dir, true)
      else
        File.open(data_file, 'w') { |f| f.write(tbl.to_csv) }
      end
      true
    end

    private

    def table
      CSV.table(data_file)
    rescue SystemCallError
      CSV::Table.new([])
    end

    def row_matches?(row, fields)
      row.fields(*fields.keys) == fields.values
    end
  end
end
