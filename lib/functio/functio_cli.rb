#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'thor'
require 'functio/errors'
require 'functio/expression'
require 'functio/formatted_num'
require 'functio/function'
require 'functio/function_repository'
require 'functio/version'

module Functio
  # Functio command-line interface.
  class FunctioCLI < Thor
    package_name 'Functio'

    map '--version' => :version

    desc 'version', 'Show Functio version'
    def version # :nodoc:
      puts <<-EOS
Functio #{Functio::VERSION}
Copyright (C) 2016 Cassiano Rocha Kuplich
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
EOS
    end

    desc 'eval EXPRESSION', 'Evaluate EXPRESSION and return its result'
    def eval(expr) # :nodoc:
      result = Expression.new(expr).evaluate
      abort "Error: expression can't have variables" unless result
      puts FormattedNum.new(result)
    rescue DivisionByZeroError
      abort 'Error: division by zero'
    rescue InvalidExpressionError
      abort 'Error: invalid expression'
    end

    desc 'create NAME DEF',
         'Create a function called NAME defined by DEF'
    def create(name, definition) # :nodoc:
      function = Function.build(name: name, definition: definition)
    rescue DivisionByZeroError
      abort "Invalid function: division by zero '#{definition}'"
    rescue InvalidFunctionError => ex
      abort "Invalid function: #{ex.message}"
    else
      abort "Error: a function '#{name}' already exists" unless
        repository.add(function)
    end

    desc 'list', 'List all functions'
    def list # :nodoc:
      puts repository.all.map { |function| function_to_str(function) }
    end

    desc 'use FUNCTION [ARGS]', 'Use the function FUNCTION with ARGS'
    def use(function_name, *args) # :nodoc:
      function = repository.find(function_name)
      abort "Error: function '#{function_name}' doesn't exist" unless function
      begin
        result = function.use(map_function_args(function.params, args))
        abort 'Error: no value provided for some parameters' unless result
        puts FormattedNum.new(result)
      rescue DivisionByZeroError
        abort 'Error: division by zero'
      end
    end

    desc 'delete FUNCTION', 'Delete the function called FUNCTION'
    def delete(function_name) # :nodoc:
      if repository.delete(function_name)
        puts "Deleted function '#{function_name}'"
      else
        abort "Error: function '#{function_name}' doesn't exist"
      end
    end

    private

    def repository
      @repository ||= FunctionRepository.new
    end

    def function_to_str(function)
      "#{function.name} <#{function.params.join('> <')}>"
    end

    def map_function_args(params, args)
      args.map.with_index { |arg, i| [params[i], arg] }.to_h
    end
  end
end
