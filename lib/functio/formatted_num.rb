#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

module Functio
  # Represents a number using a default format.
  class FormattedNum
    # Creates a FormattedNum instance from a +num_obj+.
    def initialize(num_obj)
      @num = num_obj
    end

    # Converts the FormattedNum to a String. The String is formatted using
    # conventional floating point notation if the number is a float.
    def to_s
      case @num
      when BigDecimal
        @num.to_s('F')
      else
        @num.to_s
      end
    end
  end
end
