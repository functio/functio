#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'functio/errors'
require 'functio/expression'

module Functio
  # Represents a function.
  class Function
    # Maximum size of the function name.
    MAX_NAME_SIZE = 20

    # Pattern for valid function name format.
    NAME_PATTERN = /\A[a-zA-Z][a-zA-Z0-9]*\z/

    # Function name.
    attr_reader :name

    # Expression instance used for Function definition.
    attr_reader :expression

    # Builds a new Function instance by Function +attributes+. +attributes+ is a
    # Hash containing +:name+ and +:definition+. Raises InvalidFunctionError if
    # some of the +attributes+ are invalid. Raises DivisionByZeroError if
    # +:definition+ contains a division by zero.
    def self.build(attributes)
      expression = Expression.new(attributes[:definition])
      new(name: attributes[:name], expression: expression)
    rescue InvalidExpressionError
      raise InvalidFunctionError, 'definition must be a valid expression'
    end

    # Constructs a Function instance. The parameter +args+ is a Hash containing
    # the function +:name+ and an +:expression+ instance.
    def initialize(args)
      @name = validate_name(args[:name])
      @expression = args[:expression]
    end

    # Function definition.
    def definition
      expression.expression
    end

    # Function attributes.
    def attributes
      { name: name, definition: definition }
    end

    # Returns the function input parameters in an Array of Strings.
    def params
      expression.variables
    end

    # Uses function with +args+ Hash. The keys of +args+ Hash are the input
    # parameters and the values are the input arguments.
    def use(args = {})
      expression.evaluate(args)
    end

    # Compares equality between Function instances. Returns +true+ if
    # Function#attributes of this instance is equal to Function#attributes of
    # +other+.
    def ==(other)
      attributes == other.attributes
    end

    private

    def validate_name(name)
      raise InvalidFunctionError,
            "name size can't be greater than #{MAX_NAME_SIZE} characters" unless
        valid_name_size?(name)
      raise InvalidFunctionError,
            'name must be alphanumeric and begin with a letter' unless
        valid_name_format?(name)
      name
    end

    def valid_name_size?(name)
      name.size <= MAX_NAME_SIZE
    end

    def valid_name_format?(name)
      name =~ NAME_PATTERN
    end
  end
end
