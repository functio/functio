#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

module Functio
  # Base exception class for Functio errors.
  class FunctioError < StandardError
    # Original exception.
    attr_reader :original

    # Constructs a new FunctioError instance passing in a +message+ and
    # optionally the +original+ exception which caused this.
    def initialize(message = nil, original = $ERROR_INFO)
      super(message)
      @original = original
    end
  end

  # Indicates that a function is invalid.
  class InvalidFunctionError < FunctioError; end

  # Indicates that an expression is invalid.
  class InvalidExpressionError < FunctioError; end

  # Indicates a division by zero.
  class DivisionByZeroError < FunctioError
    # Constructs a DivisionByZeroError instance passing in a +message+.
    def initialize(message = 'division by zero')
      super(message)
    end
  end
end
