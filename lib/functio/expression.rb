#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'dentaku'
require 'functio/errors'

module Functio
  # Represents a mathematical expression.
  class Expression
    # String representation of Expression instance.
    attr_reader :expression

    # The calculator instance used for expression evaluation.
    attr_reader :calculator

    # Constructs an Expression instance passing in an +expression+ String.
    # Raises InvalidExpressionError if +expression+ is invalid. Raises
    # DivisionByZeroError if +expression+ contains a division by zero.
    def initialize(expression)
      raise RuntimeError if expression.strip.empty?
      @calculator = Dentaku::Calculator.new
      calculator.evaluate(expression)
      @expression = expression
    rescue Dentaku::ParseError, RuntimeError
      raise InvalidExpressionError, 'invalid expression'
    rescue Dentaku::ZeroDivisionError
      raise DivisionByZeroError
    end

    # Evaluates the expression passing in its +variables+ in a Hash which keys
    # are the variables names and values the variables values. Raises
    # DivisionByZeroError if a division by zero occurs. Returns +nil+ if no
    # value is provided in +variables+ for some of the expression variables.
    def evaluate(variables = {})
      calculator.evaluate(expression, variables)
    rescue Dentaku::ZeroDivisionError
      raise DivisionByZeroError
    end

    # Returns the variables of the expression in an Array of Strings.
    def variables
      calculator.dependencies(expression)
    end
  end
end
