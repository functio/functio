#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'functio/data_storage'
require 'functio/function'

module Functio
  # Repository for function management.
  class FunctionRepository
    # Symbol with the name of the attribute that identifies a function instance.
    attr_reader :id_attr

    # Storage instance used for function persistence.
    attr_reader :storage

    # Class of the function instances this repository manages.
    attr_reader :function_class

    # Constructs a FunctionRepository instance passing in a +configs+ Hash. The
    # +configs+ Hash can contain a +:storage+ instance, defaults to a
    # DataStorage instance; a +:function_class+, defaults to Function; and an
    # +:id_attr+ Symbol with a name of the attribute that identifies the
    # function instances, defaults to +:name+.
    def initialize(configs = {})
      @storage = configs.fetch(:storage, DataStorage.new)
      @function_class = configs.fetch(:function_class, Function)
      @id_attr = configs.fetch(:id_attr, :name)
    end

    # Adds a +function+ instance to repository and returns +true+. It doesn't
    # add the function and returns +false+ if its +id_attr+ already exists in
    # the repository.
    def add(function)
      return false if find(function.attributes[id_attr])
      storage.store(function.attributes)
      true
    end

    # Retrieves all function instances in the repository.
    def all
      storage.all.map { |record| function_class.build(record) }
    end

    # Finds a function instance by its +id+ attribute. Returns +nil+ otherwise.
    def find(id)
      found = storage.find(id_attr => id)
      function_class.build(found) if found
    end

    # Deletes a function instance from the repository by its +id+ attribute.
    # Returns +true+ if the function instance is deleted successfully. Returns
    # +false+ otherwise.
    def delete(id)
      storage.delete(id_attr => id)
    end
  end
end
