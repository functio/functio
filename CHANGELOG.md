# Change Log

## v0.1.1 (2016-09-02)

- Establish the set of mathematical operations and functions to be the same set
of Dentaku version 2.0.8.

## v0.1.0 (2016-05-22)

- Initial release. Features:
  - Expression evaluation (command `eval`).
  - Function management (commands `create`, `list`, `use`, and `delete`).
  - Command `help` for Functio usage help.
  - Command `version` to show Functio version.
