#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'minitest/autorun'
require 'papers'

# Configures Papers for license compatibility validation.
Papers.configure do |config|
  # A whitelist of accepted licenses.
  config.license_whitelist = ['MIT', 'BSD', 'BSD-2-Clause', 'BSD-3-Clause',
                              'Ruby', 'GPL-3.0+']

  # The location of your dependency manifest. Defaults to
  # config/papers_manifest.yml
  config.manifest_file = File.join('config', 'papers_manifest.yml')

  # Configures Papers to validate licenses for bundled gems. Defaults to true.
  config.validate_gems = true

  # Configures Papers to validate licenses for included JavaScript and
  # CoffeScript files. Defaults to true.
  config.validate_javascript = false

  # Configures Papers to validate licenses for bower components. Defaults to
  # false.
  config.validate_bower_components = false

  # Configures Papers to validate licenses for NPM dependencies. Defaults to
  # false.
  config.validate_npm_packages = false

  # Configured Papers to ignore NPM dev dependencies. Defaults to false.
  config.ignore_npm_dev_dependencies = false
end
