#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'test_helper'
require 'functio/data_storage'
require 'fileutils'
require 'storage_interface_test'

class TestDataStorage < Minitest::Test # :nodoc:
  include StorageInterfaceTest

  def setup
    @real_home = ENV['HOME']

    ENV['HOME'] = File.join('/tmp', 'fake_home')
    @default_data_file = File.join(ENV['HOME'], '.functio', 'functions.csv')

    FileUtils.remove_dir(ENV['HOME'], true)
    FileUtils.mkdir_p(ENV['HOME'])

    @object = @storage = DataStorage.new
  end

  def teardown
    ENV['HOME'] = @real_home
  end

  def test_store
    @storage.store(name: 'first', definition: 'record')
    first = "name,definition\nfirst,record\n"

    assert File.exist?(@default_data_file)
    assert_equal(first, File.read(@default_data_file))

    @storage.store(name: 'second', definition: 'record')
    assert_equal(first + "second,record\n", File.read(@default_data_file))
  end

  def test_find_stored_record
    set_fixture_csv
    expected = { name: 'first', definition: 'one' }
    assert_equal(expected, @storage.find(name: 'first'))
    assert_equal(expected, @storage.find(definition: 'one'))
    assert_equal(expected, @storage.find(name: 'first', definition: 'one'))

    expected = { name: 'second', definition: 'two' }
    assert_equal(expected, @storage.find(name: 'second'))
    assert_equal(expected, @storage.find(definition: 'two'))
    assert_equal(expected, @storage.find(name: 'second', definition: 'two'))
  end

  def test_find_inexistent_record
    assert_nil(@storage.find(name: 'first'))
    assert_nil(@storage.find(definition: 'two'))

    set_fixture_csv
    assert_nil(@storage.find(name: 'something else'))
    assert_nil(@storage.find(definition: 'something else'))
    assert_nil(@storage.find(name: 'first', definition: 'two'))
    assert_nil(@storage.find(other: 'test'))
  end

  def test_get_all_records
    assert_equal([], @storage.all)

    set_fixture_csv
    all_records = @storage.all
    assert_equal(2, all_records.size)
    assert_includes(all_records, name: 'first', definition: 'one')
    assert_includes(all_records, name: 'second', definition: 'two')
  end

  def test_delete_stored_record
    set_fixture_csv

    expected = "name,definition\nsecond,two\n"
    assert @storage.delete(name: 'first')
    assert_equal(expected, File.read(@default_data_file))

    assert @storage.delete(definition: 'two', name: 'second')
    refute File.exist?(@default_data_file)
  end

  def test_delete_from_empty_data
    refute @storage.delete(name: 'something', definition: 'else')
  end

  def test_delete_inexistent_record
    set_fixture_csv

    expected = "name,definition\nfirst,one\nsecond,two\n"
    fields_cases = [{ name: 'something else' },
                    { definition: 'something else' },
                    { other: 'something else' }]
    fields_cases.each do |fields|
      refute @storage.delete(fields)
      assert_equal(expected, File.read(@default_data_file))
    end
  end

  private

  def set_fixture_csv
    FileUtils.mkdir_p(File.dirname(@default_data_file))
    File.open(@default_data_file, 'a') do |f|
      f.puts 'name,definition'
      f.puts 'first,one'
      f.puts 'second,two'
    end
  end
end
