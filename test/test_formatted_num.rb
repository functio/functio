#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'test_helper'
require 'bigdecimal'
require 'functio/formatted_num'

class TestFormattedNum < Minitest::Test # :nodoc:
  def test_fixnum_format
    test_data = [[123, '123'], [0, '0'], [-123, '-123']]
    test_data.each do |input, expected|
      assert_instance_of(Fixnum, input)
      assert_equal(expected, FormattedNum.new(input).to_s)
    end
  end

  def test_bignum_to_string
    test_data = [
      [12_345_678_901_234_567_890, '12345678901234567890'],
      [-12_345_678_901_234_567_890, '-12345678901234567890']
    ]
    test_data.each do |input, expected|
      assert_instance_of(Bignum, input)
      assert_equal(expected, FormattedNum.new(input).to_s)
    end
  end

  def test_bigdecimal_to_string
    test_data = [
      [BigDecimal.new('3.14'), '3.14'],
      [BigDecimal.new('-3.14'), '-3.14'],
      [BigDecimal.new('1234567.8901234567890'), '1234567.890123456789'],
      [BigDecimal.new('0.00000001'), '0.00000001']
    ]
    test_data.each do |input, expected|
      assert_instance_of(BigDecimal, input)
      assert_equal(expected, FormattedNum.new(input).to_s)
    end
  end
end
