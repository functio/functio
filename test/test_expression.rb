#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'test_helper'
require 'functio/errors'
require 'functio/expression'
require 'expression_interface_test'

class TestExpression < Minitest::Test # :nodoc:
  include ExpressionInterfaceTest

  def setup
    @object = Expression.new('42')
  end

  def test_instantiate_with_invalid_expressions
    exprs = ['1 +', '3 $ 5', '1 2 3', '#@!', '', ' ', '  ']
    exprs.each do |expr|
      assert_raises(InvalidExpressionError) { Expression.new(expr) }
    end
  end

  def test_instantiate_with_division_by_zero
    exprs = ['1 / 0', '1 / (1 - 1)']
    exprs.each do |expr|
      assert_raises(DivisionByZeroError) do
        Expression.new(expr)
      end
    end
  end

  def test_evaluate_expressions_with_no_variables
    tests = { '1 + 1' => 2, '3.14 * 2' => 6.28, '42' => 42, '2 * (3 + 5)' => 16,
              '-2 + 1' => -1 }
    tests.each do |expr, expected|
      assert_equal(expected, Expression.new(expr).evaluate)
    end
  end

  def test_evaluate_expressions_with_variables
    tests = { ['a + b', { a: 1, b: 2 }] => 3,
              ['x * y', { x: 2, y: 3.14 }] => 6.28,
              ['const', { const: 42 }] => 42 }
    tests.each do |inputs, expected|
      expression = Expression.new(inputs[0])
      assert_equal(expected, expression.evaluate(inputs[1]))
    end
  end

  def test_evaluate_with_wrong_list_of_variables
    tests = { 'x' => {}, 'a + b' => { a: 1 }, 'x + y' => { y: 1 },
              'y' => { x: 1 } }
    tests.each do |expr, vars|
      expression = Expression.new(expr)
      assert_nil(expression.evaluate(vars))
    end
  end

  def test_evaluate_division_by_zero
    expression = Expression.new('1 / x')
    assert_raises(DivisionByZeroError) do
      expression.evaluate(x: 0)
    end
  end

  def test_variables
    tests = { 'a + b' => %w(a b), 'x' => ['x'], 'b / (a + b) * c' => %w(b a c),
              '42' => [] }
    tests.each do |expr, expected|
      assert_equal(expected, Expression.new(expr).variables)
    end
  end
end
