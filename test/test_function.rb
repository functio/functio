#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'test_helper'
require 'doubles/expression_double'
require 'functio/errors'
require 'functio/function'
require 'storable_interface_test'

class TestFunction < Minitest::Test # :nodoc:
  include StorableInterfaceTest

  def setup
    @object = Function.new(name: 'test', expression: ExpressionDouble.new)
  end

  def test_build_function
    function = Function.build(name: 'succ', definition: 'x + 1')
    assert_instance_of(Function, function)
    assert_equal('succ', function.name)
    assert_equal('x + 1', function.definition)
  end

  def test_build_with_invalid_name
    ['1abc', 'ab c', '+!@#'].each do |name|
      ex = assert_raises(InvalidFunctionError) do
        Function.build(name: name, definition: 'x + 1')
      end
      assert_equal('name must be alphanumeric and begin with a letter',
                   ex.message)
    end
  end

  def test_build_with_too_long_name
    ex = assert_raises(InvalidFunctionError) do
      Function.build(name: 'abcdefghijklmnopqrstu', definition: 'x + 1')
    end
    assert_equal(
      "name size can't be greater than #{Function::MAX_NAME_SIZE} characters",
      ex.message)
  end

  def test_build_with_invalid_definition
    ex = assert_raises(InvalidFunctionError) do
      Function.build(name: 'invalid', definition: 'x +')
    end
    assert_equal('definition must be a valid expression', ex.message)
  end

  def test_build_with_division_by_zero
    ex = assert_raises(DivisionByZeroError) do
      Function.build(name: 'divbyzero', definition: '1 / 0')
    end
    assert_equal('division by zero', ex.message)
  end

  def test_instantiate_with_invalid_name
    ['1abc', 'ab c', '+!@#'].each do |name|
      ex = assert_raises(InvalidFunctionError) do
        expr_stub = ExpressionDouble.new(expression: 'a * b')
        Function.new(name: name, expression: expr_stub)
      end
      assert_equal('name must be alphanumeric and begin with a letter',
                   ex.message)
    end
  end

  def test_instantiate_with_too_long_name
    ex = assert_raises(InvalidFunctionError) do
      expr_stub = ExpressionDouble.new(expression: 'a * b')
      Function.new(name: 'abcdefghijklmnopqrstu', expression: expr_stub)
    end
    assert_equal(
      "name size can't be greater than #{Function::MAX_NAME_SIZE} characters",
      ex.message)
  end

  def test_attributes
    expr_stub = ExpressionDouble.new(expression: 'a * b')
    function = Function.new(name: 'test', expression: expr_stub)
    assert_equal({ name: 'test', definition: 'a * b' }, function.attributes)
  end

  def test_equals_operator
    add = function('add', 'a + b')
    assert add == function('add', 'a + b')
    refute add == function('sum', 'a + b')
    refute add == function('add', 'x + y')
    refute add == function('sum', 'x + y')
  end

  def test_function_params
    expr_stub = ExpressionDouble.new(variables: %w(x y z))
    function = Function.new(name: 'test', expression: expr_stub)
    assert_equal(%w(x y z), function.params)
  end

  def test_use_function
    expr_stub = ExpressionDouble.new(evaluate: -> { 42 })
    function = Function.new(name: 'test', expression: expr_stub)
    assert_equal(42, function.use('x' => 1))
  end

  def test_use_with_invalid_args
    expr_stub = ExpressionDouble.new(evaluate: -> { nil })
    function = Function.new(name: 'test', expression: expr_stub)
    assert_equal(nil, function.use('x' => 1))
    assert_equal(nil, function.use)
  end

  def test_use_with_division_by_zero
    expr_stub = ExpressionDouble.new(evaluate: lambda do
      raise DivisionByZeroError, 'division by zero'
    end)
    function = Function.new(name: 'test', expression: expr_stub)
    ex = assert_raises(DivisionByZeroError) { function.use }
    assert_equal('division by zero', ex.message)
  end

  private

  def function(name, definition)
    expr = ExpressionDouble.new(expression: definition)
    Function.new(name: name, expression: expr)
  end
end
