#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'interface_test_helper'

module StorableInterfaceTest # :nodoc:
  include InterfaceTestHelper

  def test_implements_the_storable_interface
    assert_implements(@object.class, :build, 1)
    assert_implements(@object, :attributes, 0)
    assert_implements(@object, :==, 1)
  end
end
