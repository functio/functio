#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'test_helper'
require 'doubles/function_double'
require 'doubles/storage_double'
require 'functio/function_repository'

class TestFunctionRepository < Minitest::Test # :nodoc:
  def test_add_new_function
    storage_mock = Minitest::Mock.new
    def storage_mock.find(_fields)
      nil
    end
    storage_mock.expect :store, :return, [{ name: 'test', definition: 'a + b' }]
    repo = FunctionRepository.new(storage: storage_mock)
    function = FunctionDouble.build(name: 'test', definition: 'a + b')

    assert repo.add(function)

    storage_mock.verify
  end

  def test_add_function_with_existent_id
    storage_double = StorageDouble.new(
      find: { name: 'test', definition: 'a * b' })
    repo = FunctionRepository.new(storage: storage_double)
    function = FunctionDouble.build(name: 'test', definition: 'a + b')

    refute repo.add(function)
  end

  def test_get_all_functions
    stored = [{ name: 'add', definition: 'a + b' },
              { name: 'sub', definition: 'a - b' }]
    storage_double = StorageDouble.new(all: stored)
    repo = FunctionRepository.new(storage: storage_double,
                                  function_class: FunctionDouble)
    expected = [FunctionDouble.build(stored[0]),
                FunctionDouble.build(stored[1])]
    assert_equal(expected, repo.all)
  end

  def test_get_all_with_empty_repository
    storage_double = StorageDouble.new(all: [])
    repo = FunctionRepository.new(storage: storage_double,
                                  function_class: FunctionDouble)
    assert_equal([], repo.all)
  end

  def test_find_function
    record = { name: 'test', definition: 'a + b' }
    storage_double = StorageDouble.new(find: record)
    repo = FunctionRepository.new(storage: storage_double,
                                  function_class: FunctionDouble)
    assert_equal(FunctionDouble.build(record), repo.find('test'))
  end

  def test_find_inexistent_function
    storage_double = StorageDouble.new(find: nil)
    repo = FunctionRepository.new(storage: storage_double,
                                  function_class: FunctionDouble)
    assert_equal(nil, repo.find('test'))
  end

  def test_delete_function
    storage_mock = Minitest::Mock.new
    storage_mock.expect :delete, true, [{ name: 'test' }]
    repo = FunctionRepository.new(storage: storage_mock)

    assert repo.delete('test')
    storage_mock.verify
  end

  def test_delete_inexistent_function
    storage_double = StorageDouble.new(delete: false)
    repo = FunctionRepository.new(storage: storage_double)

    refute repo.delete('test')
  end
end
