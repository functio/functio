#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'aruba/cucumber'
require 'fileutils'
require 'functio/function'

include Functio

# Configure Aruba to set $HOME environment variable to ./tmp/aruba
Aruba.configure do |config|
  config.home_directory = File.join(config.root_directory,
                                    config.working_directory)
end

# ENV['HOME'] for Cucumber steps must be set too because Cucumber steps runs in
# another process.
Before do
  @real_home = ENV['HOME']
  ENV['HOME'] = Aruba.config.home_directory
end

After do
  ENV['HOME'] = @real_home
end
