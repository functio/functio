#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'minitest/spec'
require 'functio/function_repository'

# Custom Cucumber World with Minitest assertions and Functio objects.
class FunctioWorld
  include Minitest::Assertions
  include Functio

  attr_accessor :assertions # :nodoc:

  def initialize # :nodoc:
    self.assertions = 0
  end

  def data_dir # :nodoc:
    function_repo.storage.data_dir
  end

  def function_repo # :nodoc:
    @function_repo ||= FunctionRepository.new
  end
end

World do
  FunctioWorld.new
end
