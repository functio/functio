Feature: Version option
  In order to know the current version of Functio
  As a User
  I want an option to show current Functio version

  Scenario Outline: Run with version option
    When I successfully run `<command>`
    Then the output should match:
    """
    Functio \d+\.\d+\.\d+
    Copyright \(C\) 2016 Cassiano Rocha Kuplich
    License GPLv3\+: GNU GPL version 3 or later \<http://gnu.org/licenses/gpl\.html\>\.
    This is free software: you are free to change and redistribute it\.
    There is NO WARRANTY, to the extent permitted by law\.
    """
    Examples:
    | command      |
    | fn --version |
    | fn version   |
