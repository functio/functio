#--
# Copyright (C) 2016  Cassiano Rocha Kuplich
#
# This file is part of Functio.
#
# Functio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Functio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Functio.  If not, see <http://www.gnu.org/licenses/>.
#++

Given(/^I have no functions$/) do
  FileUtils.remove_dir(data_dir, true)
end

Given(/^I have the functions?:$/) do |table|
  FileUtils.remove_dir(data_dir, true)
  table.symbolic_hashes.each do |function_args|
    function_repo.add(Function.build(function_args))
  end
end

Then(/^the following functions? should be available:$/) do |table|
  functions = table.symbolic_hashes.map do |function_args|
    Function.build(function_args)
  end
  assert_equal(functions, function_repo.all)
end
