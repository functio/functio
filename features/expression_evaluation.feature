Feature: Expression evaluation
  In order to know the result of a calculation
  As a User
  I want to write a math expression and get its result

  Scenario Outline: Valid math expressions
    When I run `fn eval '<expression>'`
    Then it should pass with "<result>"
    Examples:
    | expression | result |
    | 1 + 1      | 2      |
    | 1 + 2 * 3  | 7      |

  Scenario Outline: Default result format
    When I run `fn eval '<expression>'`
    Then it should pass with exactly:
    """
    <result>
    """
    Examples:
    | expression            | result               |
    | 123                   | 123                  |
    | 12345678901234567890  | 12345678901234567890 |
    | 3.14                  | 3.14                 |
    | 123456.78901234567890 | 123456.7890123456789 |

  Scenario Outline: Invalid math expressions
    When I run `fn eval '<expression>'`
    Then it should fail with:
    """
    Error: invalid expression
    """
    Examples:
    | expression |
    | (1 + 2     |
    | 2 * 3)     |
    | 2 $ 3      |
    | 2 *        |
    | 1 2 3      |

  Scenario: Expression with variable
    When I run `fn eval 'var + 3'`
    Then it should fail with:
    """
    Error: expression can't have variables
    """

  Scenario Outline: Division by zero
    When I run `fn eval '<expression>'`
    Then it should fail with:
    """
    Error: division by zero
    """
    Examples:
    | expression  |
    | 1 / 0       |
    | 2 / (1 - 1) |
    | 3 * 2 / 0   |
