Feature: Help option
  In order to learn how to use Functio
  As a User
  I want a help option that shows Functio usage

  Scenario Outline: Run with help option
    When I successfully run `<command>`
    Then the output should contain "Functio commands"
    Examples:
    | command   |
    | fn        |
    | fn --help |
    | fn help   |
