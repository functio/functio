Feature: Function list
  In order to know what functions are available
  As a User
  I want to list my functions

  Scenario: No functions available
    Given I have no functions
    When I successfully run `fn list`
    Then the output should not contain anything

  Scenario: One function created
    Given I have the function:
    | name    | definition                |
    | product | multiplier * multiplicand |
    When I run `fn list`
    Then it should pass with:
    """
    product <multiplier> <multiplicand>
    """

  Scenario: More functions available
    Given I have the functions:
    | name       | definition                |
    | sum        | augend + addend           |
    | difference | minuend - subtrahend      |
    | product    | multiplier * multiplicand |
    | quotient   | dividend / divisor        |
    When I run `fn list`
    Then it should pass with:
    """
    sum <augend> <addend>
    difference <minuend> <subtrahend>
    product <multiplier> <multiplicand>
    quotient <dividend> <divisor>
    """
