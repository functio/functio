Feature: Function creation
  In order to do recurrent calculations
  As a User
  I want to create custom functions

  Scenario: Valid function creation
    Given I have no functions
    When I successfully run `fn create fahr2celsius '(f - 32) * 5 / 9'`
    Then the output should not contain anything
    And the following function should be available:
    | name         | definition       |
    | fahr2celsius | (f - 32) * 5 / 9 |

  Scenario: Function with the same name of another function
    Given I have the function:
    | name | definition |
    | add  | a + b      |
    When I run `fn create add 'x + y'`
    Then it should fail with:
    """
    Error: a function 'add' already exists
    """

  Scenario Outline: Invalid function name
    Given I have no functions
    When I run `fn create '<name>' 'x + 1'`
    Then it should fail with:
    """
    Invalid function: name must be alphanumeric and begin with a letter
    """
    Examples:
    | name       |
    | 1plus      |
    | x plus one |
    | x+one      |
    | plusone!   |

  Scenario: Function name is too long
    Given I have no functions
    When I run `fn create abcdefghijklmnopqrstu 'a + b'`
    Then it should fail with:
    """
    Invalid function: name size can't be greater than 20 characters
    """

  Scenario Outline: Invalid function definition
    Given I have no functions
    When I run `fn create myfunction <definition>`
    Then it should fail with:
    """
    Invalid function: definition must be a valid expression
    """
    Examples:
    | definition  |
    | '1 +'       |
    | '(a + b'    |
    | 'a * b)'    |
    | '# &'       |
    | '* x'       |
    | ''          |
    | ' '         |
    | '  '        |

  Scenario Outline: Definition with division by zero
    Given I have no functions
    When I run `fn create myfunction '<definition>'`
    Then it should fail with:
    """
    Invalid function: division by zero '<definition>'
    """
    Examples:
    | definition  |
    | a / 0       |
    | a / (2 - 2) |
