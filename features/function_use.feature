Feature: Function use
  In order to do my recurrent calculations quickly
  As a User
  I want to use previously created functions

  Scenario Outline: Successful use of an available function
    Given I have the function:
    | name | definition |
    | add  | a + b      |
    When I run `fn use add <a> <b>`
    Then it should pass with:
    """
    <result>
    """
    Examples:
    | a    | b   | result |
    | 1    | 2   | 3      |
    | -1   | 2   | 1      |
    | 0    | 0   | 0      |
    | 3    | -1  | 2      |
    | 3    | 2.5 | 5.5    |
    | 40.5 | 1.5 | 42     |

  Scenario Outline: Wrong number of function arguments
    Given I have the function:
    | name | definition |
    | add  | a + b      |
    When I run `fn use add <args>`
    Then it should fail with:
    """
    Error: no value provided for some parameters
    """
    Examples:
    | args |
    |      |
    | 1    |

  Scenario: Inexistent function
    Given I have the function:
    | name | definition |
    | add  | a + b      |
    When I run `fn use subtract 2 1`
    Then it should fail with:
    """
    Error: function 'subtract' doesn't exist
    """

  Scenario: Runtime function calculation error
    Given I have the function:
    | name   | definition |
    | divide | a / b      |
    When I run `fn use divide 1 0`
    Then it should fail with:
    """
    Error: division by zero
    """
