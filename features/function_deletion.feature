Feature: Function deletion
  In order to remove an unnecessary function
  As a User
  I want to delete a function

  Scenario: Available function
    Given I have the functions:
    | name     | definition |
    | add      | a + b      |
    | multiply | a * b      |
    When I run `fn delete add`
    Then it should pass with:
    """
    Deleted function 'add'
    """
    And the following function should be available:
    | name     | definition |
    | multiply | a * b      |

  Scenario: No functions available
    Given I have no functions
    When I run `fn delete add`
    Then it should fail with:
    """
    Error: function 'add' doesn't exist
    """

  Scenario: Function doesn't exist
    Given I have the functions:
    | name     | definition |
    | add      | a + b      |
    | multiply | a * b      |
    When I run `fn delete subtract`
    Then it should fail with:
    """
    Error: function 'subtract' doesn't exist
    """
